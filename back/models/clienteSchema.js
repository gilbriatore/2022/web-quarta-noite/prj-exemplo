const mongoose = require('mongoose');
const

const clienteSchema = mongoose.Schema({
    codigo: Number,
    nome: String,
    desc: String,
    preco: Number,
    loja: {type: mongoose.Schema.Types.ObjectId, ref: 'loja'}
});

module.exports = clienteSchema;