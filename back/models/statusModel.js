const mongoose = require('mongoose');

const statusSchema = mongoose.Schema({
    codigo: Number,
    nome: String,
});

module.exports = mongoose.model('status', statusSchema);