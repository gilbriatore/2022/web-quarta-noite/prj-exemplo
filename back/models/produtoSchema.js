const mongoose = require('mongoose');
const

const produtoSchema = mongoose.Schema({
    codigo: Number,
    nome: String,
    desc: String,
    preco: Number,
    categoria: {type: mongoose.Schema.Types.ObjectId, ref: 'categoria'}
});

module.exports = produtoSchema;