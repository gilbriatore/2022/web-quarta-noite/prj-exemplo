const mongoose = require('mongoose');
const clienteSchema = require('./clienteSchema');
const produtoSchema = require('./produtoSchema');

const pedidoSchema = mongoose.Schema({
    codigo: Number,
    nome: String,
    total: Number,
    data: Date,
    clienteId: {type: mongoose.Schema.Types.ObjectId, ref: 'cliente'},
    statusId: {type: mongoose.Schema.Types.ObjectId, ref: 'status'},
    cliente: clienteSchema,
    itens: [produtoSchema]
});

module.exports = mongoose.model('pedido', pedidoSchema);