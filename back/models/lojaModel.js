const mongoose = require('mongoose');

const lojaSchema = mongoose.Schema({
    codigo: Number,
    nome: String,
});

module.exports = mongoose.model('loja', lojaSchema);